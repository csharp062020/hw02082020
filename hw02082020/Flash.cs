﻿using System;

namespace hw02082020
{
    public class Flash : Human, IFlash
    {
        private int _voltage; 

        public Flash(string name, int age , int volt) : base(name, age)
        {
            Voltage = volt;
        }

        public int Voltage
        {
            get
            {
                return _voltage;
            }
            private set
            {
                _voltage = value;
            }
        }
        public void ActivateSuperPowers()
        {
            FireLightnings();
        }

        public void FireLightnings()
        {
            Console.WriteLine("FireLightnings power on");
        }

        public override string ToString()
        {
            return $"{base.ToString()} FireLightnings(Voltage): {Voltage} V";
        }
    }
}
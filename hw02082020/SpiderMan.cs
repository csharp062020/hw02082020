﻿using System;

namespace hw02082020
{
    class SpiderMan : Human,IClimb
    {
        private int _speed;
        public SpiderMan(string name, int age , int speed) : base(name, age)
        {
            Speed = speed;
        }



        public int Speed
        {
            get
            {
                return _speed;
            }
            private set
            {
                _speed = value;
            }
        }
        public void Cilimb()
        {
            Console.WriteLine("Cilimb power on");
        }

        public void ActivateSuperPowers()
        {
            Cilimb();
        }

        public override string ToString()
        {
            return $"{base.ToString()} speed: {Speed}";
        }

    }
}
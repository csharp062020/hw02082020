﻿namespace hw02082020
{
    interface IFlash : ISuperHero
    {
        void FireLightnings();
    }
}
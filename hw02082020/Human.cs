﻿namespace hw02082020
{
    public abstract class Human
    {
        private string _name;
        private int _age;

        public Human(string name , int age)
        {
            this.Name = name;
            this.Age = age;
        }
        public string Name
        {
            get
            {
                return _name; 
            }
            set
            {
                _name = value;
            }
        }

        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                _age = value;
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()} name: {Name} , age: {Age}";
        }
    }
}
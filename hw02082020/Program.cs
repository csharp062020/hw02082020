﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw02082020
{
    class Program
    {
        static void Main(string[] args)
        {
            Flash flash = new Flash("f1",30,500);
            SpiderMan spiderMan = new SpiderMan("S1",29,100);
            SuperMan superMan = new SuperMan("SU1",28,60);
            Console.WriteLine("------------to string check-----------");
            Console.WriteLine($"{flash}");
            Console.WriteLine($"{spiderMan}");
            Console.WriteLine($"{superMan}");

            ISuperHero[] iSuperHeroesArray = new ISuperHero[]
            {
                flash,spiderMan,superMan
            };

            Console.WriteLine("------------------ActivateHero checked ----------------");
            Console.WriteLine("flash active");
            ActivateHero(flash);

            Console.WriteLine("spider active");
            ActivateHero(spiderMan);

            Console.WriteLine("super active");
            ActivateHero(superMan);

            Console.WriteLine($"----------active power in loop-------------");
            foreach (ISuperHero super in iSuperHeroesArray)
            {
                Console.WriteLine($"\npower is active {super.GetType()} ");
                super.ActivateSuperPowers();
            }


            Console.WriteLine($"----------detected SuperHeroe in loop-------------");

            foreach (ISuperHero super in iSuperHeroesArray)
            {
                
                Console.WriteLine($"\ndetected SuperHeroe {super.GetType()} ");
                IdentifyHero(super);
            }

            Console.WriteLine($"----------detected SuperHeroe From string to Class-------------");

            ISuperHero superHero1 = CreateHero("SpiderMan");
            ISuperHero superHero2 = CreateHero("Superman");
            ISuperHero superHero3 = CreateHero("flash");
            ISuperHero superHeroErrorString = CreateHero("flashh");

            IdentifyHero(superHero1);
            IdentifyHero(superHero2);
            IdentifyHero(superHero3);
            IdentifyHero(superHeroErrorString);
        }

        static void ActivateHero(ISuperHero superHero)
        {
            superHero.ActivateSuperPowers();
        }

        static void IdentifyHero(ISuperHero super)
        {
            // in this case  switch case is smarter choose 
            if (super is SuperMan)
            {
                Console.Write("\nSuperMan");
            }
            else if (super is SpiderMan)
            {
                Console.Write("\nSpiderMan");
            }
            else if (super is Flash)
            {
                Console.Write("\nFlash");
            }
            else
            {
                Console.Write("\nNo superHero");
            }
            Console.Write(" detected\n");
        }

        static void GetMoreHeroData(ISuperHero super)
        {
            //A more correct implementation would be through an implementation similar to ActivateSuperPowers
            SuperMan superMan = super as SuperMan;
            if (superMan != null)
            {
                Console.WriteLine($"{superMan.WebLeft}");
                return;
            }
            
            SpiderMan spiderMan = super as SpiderMan;
            if (spiderMan != null)
            {
                Console.WriteLine($"{spiderMan.Speed}");
                return;
            }

            Flash flash = super as Flash;
            if (flash != null)
            {
                Console.WriteLine($"{flash.Voltage}");
                return;
            }
        }

        static ISuperHero CreateHero(string hero)
        {
            switch (hero.ToLower())
            {
                case "spiderman":
                    return new SpiderMan("a",1,100);
                case "flash":
                    return new Flash("b", 1, 100);
                case "superman":
                    return new SuperMan("c", 1, 100);
                default:
                    return null;
            }
        }


    }
}

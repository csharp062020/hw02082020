﻿using System;

namespace hw02082020
{
    public class SuperMan : Human, IFly
    {
        private int _webLeft;

        public SuperMan(string name, int age, int webleft) : base(name, age)
        {
            WebLeft = webleft;
        }

        public int WebLeft
        {
            get
            {
                return _webLeft;
            }
            set
            {
                _webLeft = value;
            }
        }

        public void ActivateSuperPowers()
        {
            Fly();
        }

        public void Fly()
        {
            Console.WriteLine("Fly power on");
        }
        public override string ToString()
        {
            return $"{base.ToString()} fly(WebLeft): {WebLeft}";
        }
    }
}